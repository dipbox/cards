package com.dipbox.cards.domain;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class CardDeckTest {

	private CardDeck cardDeck;
	
	@Before
	public void setUp() throws Exception {
		cardDeck = new CardDeck();
	}

	@Test
	public void CardDeck_constructor() {
		assertEquals("It is 52 cards in the deck", 52, cardDeck.getCards().size());
	}

	@Test
	public void CardDeck_contains() {
		assertEquals("Deck contains the ace of hearts", true, cardDeck.getCards().contains(new Card(1, Suit.HEARTS)));
		assertEquals("Deck contains one King of spades", cardDeck.getCards().indexOf(new Card(13, Suit.SPADES)), cardDeck.getCards().lastIndexOf(new Card(13, Suit.SPADES)));
	}

	
}
