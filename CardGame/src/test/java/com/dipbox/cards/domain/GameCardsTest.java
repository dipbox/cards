package com.dipbox.cards.domain;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class GameCardsTest {


	@Test
	public void sortCardsTest() {
		GameCards gameCards = new GameCards(2);
		
		gameCards.mixCard();
		gameCards.sortCards();
		
		assertEquals("Card 1 = ace of hearts", new Card(1, Suit.HEARTS), gameCards.getCards().get(1-1));
		assertEquals("Card 13 = king of hearts", new Card(13, Suit.HEARTS), gameCards.getCards().get(13-1));

		assertEquals("Card 23 = 10 of diamons", new Card(10, Suit.DIAMONS), gameCards.getCards().get(23-1));
		assertEquals("Card 28 = 2 of clubs", new Card(2, Suit.CLUBS), gameCards.getCards().get(28-1));
		assertEquals("Card 42 = 3 of spades", new Card(3, Suit.SPADES), gameCards.getCards().get(42-1));

		assertEquals("Card 53 = ace of hearts", new Card(1, Suit.HEARTS, 1), gameCards.getCards().get(53-1));
		
	}

	@Test
	public void sortCardsConstantTest() {
		GameCards gameCards = new GameCards(2);
		
		int before = gameCards.getCards().size();
		gameCards.sortCards();

		assertEquals("Number of cards is constant after sorting", before, gameCards.getCards().size());
	}
	
}
