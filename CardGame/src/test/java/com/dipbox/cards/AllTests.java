package com.dipbox.cards;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.dipbox.cards.domain.CardDeckTest;
import com.dipbox.cards.domain.GameCardsTest;
import com.dipbox.cards.service.impl.CardServiceImplTest;

@RunWith(Suite.class)
@SuiteClasses({CardDeckTest.class, GameCardsTest.class, CardServiceImplTest.class})
public class AllTests {

}
