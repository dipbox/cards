package com.dipbox.cards.service.impl;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.dipbox.cards.domain.Card;
import com.dipbox.cards.domain.CardDeck;

public class CardServiceImplTest {

	private CardServiceImpl cardServiceImpl;
	
	@Before
	public void setUp() throws Exception {
		cardServiceImpl = new CardServiceImpl();
	}

	
	@Test
	public void TestMixCards() {
		List<Card> cards = new CardDeck().getCards();
		cards.addAll(new CardDeck().getCards());
		
		int cardsSizeBefore = cards.size();
		
		List<Card> mixedCards = cardServiceImpl.mixCards(cards);
		
		assertEquals("Number of cards is constant after mixing", cardsSizeBefore, mixedCards.size());
		
	}

}
