package com.dipbox.cards;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.process.internal.RequestScoped;

import com.dipbox.cards.domain.Card;
import com.dipbox.cards.domain.CardPublicHolder;
import com.dipbox.cards.domain.GameCards;
import com.dipbox.cards.domain.GameCardsPublicHolder;

@Path("/v1")
@RequestScoped
public class Api {

	private static GameCards gameCards;
	
	@GET
	@Path("/getnew/{numberOfDecks}")
	@Produces(MediaType.APPLICATION_JSON)	
	public GameCardsPublicHolder getNewCard(@PathParam("numberOfDecks") int numberOfDecks) {
 		gameCards = new GameCards(numberOfDecks);
		return  new GameCardsPublicHolder(gameCards);	
 	}
 
	
	@GET
	@Path("/getnextcard")
	@Produces(MediaType.APPLICATION_JSON)
	public CardPublicHolder getNextCard() {
		Card card = gameCards.getNexCard();
		return  new CardPublicHolder(card);
	}
	
	@GET
	@Path("/mixcards")
	@Produces(MediaType.APPLICATION_JSON)
	public GameCardsPublicHolder shuffleCard() {
		gameCards.mixCard();
		return  new GameCardsPublicHolder(gameCards);	
	}
	
	
	@GET
	@Path("/sortcards")
	@Produces(MediaType.APPLICATION_JSON)
	public GameCardsPublicHolder sortCards() {
		gameCards.sortCards();
		return  new GameCardsPublicHolder(gameCards);	
	}	
	
		
}
