package com.dipbox.cards.domain;

public class GameCardsPublicHolder {

	public int numberOfCards = 0; 
	public int nextCardNr = 0;
	public boolean inOrder;
	
	
	public GameCardsPublicHolder(GameCards gameCards) {
		this.numberOfCards = gameCards.getNumberOfCards();
		this.nextCardNr = gameCards.getNextCardNr();
		this.inOrder = gameCards.isInOrder();
	}
	
}
