package com.dipbox.cards.domain;

public class CardPublicHolder {

	public int value;
	public String displayName;
	
	public String suit;
	public String suitColor;
	public String suitHtmlChar;
	
	public int deckId;

	
	public CardPublicHolder(Card card){
		this.value = card.getValue();
		this.displayName = card.getValueName();
		this.suit = card.getSuit().name();
		this.suitColor = card.getSuit().getColor();
		this.suitHtmlChar = card.getSuit().getHtmlChar();
		this.deckId = card.getDeckId();
	}
	
	
}
