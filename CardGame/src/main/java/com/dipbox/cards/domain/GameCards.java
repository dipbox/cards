package com.dipbox.cards.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.dipbox.cards.service.CardService;
import com.dipbox.cards.service.impl.CardServiceImpl;

public class GameCards {

	private List<Card> cards;
	private CardService cardService;
	private int numberOfCards = 0; 
	private int nextCardNr = 0;
	private boolean inOrder;
	
	public GameCards(int numberOfDecks) {
		cardService = new CardServiceImpl();
		cards = new ArrayList<Card>(numberOfDecks * 52);
		for (int i = 0; i < numberOfDecks; i++) {
			cards.addAll(new CardDeck(i).getCards());
		}
		sortCards();
		numberOfCards = cards.size();
	}

	public void sortCards(){
		nextCardNr = 0;
		Collections.sort(cards, new GameCardSort());
		inOrder = true;
	}
	
	public int getNumberOfCards() {
		return numberOfCards;
	}

	public int getNextCardNr() {
		return nextCardNr;
	}

	public boolean isInOrder() {
		return inOrder;
	}

	public void mixCard(){
		nextCardNr = 0;
		cards = cardService.mixCards(cards);
		inOrder = false;
	}
	
	
	public Card getNexCard(){
		if (cards.size() > nextCardNr){
			Card card = cards.get(nextCardNr);
			nextCardNr = nextCardNr + 1;
			return card;
		}
		return null;
	}
	
	public List<Card> getCards() {
		return cards;
	}


	@Override
	public String toString() {
		return "GameCards [Number of cards=" + numberOfCards + "]";
	}


	private class GameCardSort implements Comparator<Card> {
		public int compare(Card card1, Card card2) {
			return ((card1.getDeckId()*52)+(card1.getSuit().getOrder()*13)+card1.getValue())-((card2.getDeckId()*52)+(card2.getSuit().getOrder()*13)+card2.getValue());
		}
	}
}
