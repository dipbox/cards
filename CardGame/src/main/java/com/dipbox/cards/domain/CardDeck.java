package com.dipbox.cards.domain;

import java.util.ArrayList;
import java.util.List;

public class CardDeck {
	
	private List<Card> cards;
	private int id = 0;
	
	public CardDeck(){
		this(0);
	}
	
	public CardDeck(int id){
		this.id = id;
		cards = new ArrayList<Card>(52);
		for (Suit suit : Suit.values()) {
			for (int value = 1; value <= 13; value++) {
				cards.add(new Card(value, suit, id));
			}
		}
	}
	
	public List<Card> getCards() {
		return cards;
	}

	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
}
