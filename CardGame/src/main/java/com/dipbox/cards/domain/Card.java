package com.dipbox.cards.domain;


public class Card {

	private final int value;
	private final Suit suit;
	private final int deckId;
	
	protected Card(int value, Suit suit) {
		this(value, suit, 0);
	}

	protected Card(int value, Suit suit, int deckID) {
		this.value = value;
		this.suit = suit;
		this.deckId = deckID;
	}

	public int getValue() {
		return value;
	}

	public Suit getSuit() {
		return suit;
	}

	public int getDeckId() {
		return deckId;
	}
	
	public String getValueName(){
		switch (value) {
		case 1:return "A";
		case 11:return "J";
		case 12:return "Q";
		case 13:return "K";
		default:
			return String.valueOf(value);
		}	
	}

	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + deckId;
		result = prime * result + ((suit == null) ? 0 : suit.hashCode());
		result = prime * result + value;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Card other = (Card) obj;
		if (deckId != other.deckId)
			return false;
		if (suit != other.suit)
			return false;
		if (value != other.value)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Card [value=" + value + ", suit=" + suit + ", name=" + getValueName() + ", deckId=" + deckId + "]";
	}

}
