package com.dipbox.cards.domain;

public enum Suit {

	
	HEARTS(1, "&hearts;" ,"red"), DIAMONS(2, "&diams;", "red"), CLUBS(3, "&clubs;","black"), SPADES(4, "&spades;","black");

	private int order;
	private String  color;
	private String htmlChar;
	
	
	Suit(int order, String htmlChar, String color){
		this.order = order;
		this.htmlChar = htmlChar;
		this.color = color;
	}


	public int getOrder() {
		return order;
	}


	public String getColor() {
		return color;
	}
	
	public String getHtmlChar() {
		return htmlChar;
	}
	
}
