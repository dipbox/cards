package com.dipbox.cards.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.dipbox.cards.domain.Card;
import com.dipbox.cards.service.CardService;

public class CardServiceImpl implements CardService {

	public List<Card> mixCards(List<Card> cards) {
		Random random = new Random();
		List<Card> tempCards = new ArrayList<Card>(cards);
		List<Card> mixedCards = new ArrayList<Card>(cards.size());
		
		 while(!tempCards.isEmpty()) {
			 mixedCards.add(tempCards.remove(random.nextInt(tempCards.size())));
		 }

		 return mixedCards;
	}

}
