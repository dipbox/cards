package com.dipbox.cards.service;

import java.util.List;

import com.dipbox.cards.domain.Card;

public interface CardService {

	/**
	 * Returns a new List that include the same cards in a random order
	 * 
	 * @param cards List of cards
	 * @return a shuffled List of same cards.
	 */
	public List<Card> mixCards(List<Card> cards);
	
}
