<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Pic A Card</title>

<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/bootstrap-theme.min.css">
<link rel="stylesheet" href="css/jquery-ui.css">
 

<link href="css/style.css" rel="stylesheet">

</head>
<body>
  <div class="container">
  
<div class="row">
  <div class="col-xs-12 text-center"><h1><b>Pic A Card</b></h1></div>
</div>
  
  
<div class="row">
  <div class="col-xs-6 text-right">
		<div class="pull-right card-div">
				<div class="card" id="carddown" >
					<button type="button" class="card-backside" id="carddown-inside" >
						 
					</button>
				</div>
				
		</div>
	</div>
 <div class="col-xs-6">
		<div class="card-div">
				<div class="card" id="cardup">
					<div  class="card-frontside col-xs-12" id="cardup-inside">
					 		
  								<div class="card-corner" id="top-left"></div>
  								<div class="card-corner" id="top-right"></div>
  								<div id="cardup-inside-center"></div>
								<div class="card-corner upside-down" id="down-left"></div>					 			
					 			<div class="card-corner upside-down" id="down-right"></div>
					 		
					 	
					</div>
				</div>
		</div>
</div>
</div>


</div>

<footer class="footer">
	<div class="container">
	 <div class="col-xs-6">
			<button type="button" class="btn btn-default pull-right btn-game" id="mixbtn">Mix</button>
	</div>
	<div class="col-xs-6">
			<button type="button" class="btn btn-default btn-game" id="sortbtn">Sort</button>
	</div>      
	</div>
</footer>

    <script src="js/jquery.min.js"></script>
    <script src="js/jquery-ui.js"></script> 
    <script src="js/bootstrap.min.js"></script>
    <script src="js/cardgame.js"></script>
    
</body>
</html>