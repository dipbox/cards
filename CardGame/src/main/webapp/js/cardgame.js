
var cardAPI = "api/v1/";
var nuberOfDecks = 2;
var numberOfCardsLeft = 0;

$( document ).ready(function() {
    console.log( "ready!" );
    
    $.getJSON( cardAPI+"getnew/"+nuberOfDecks)
    .done(function( json ) {
      numberOfCardsLeft = json.numberOfCards;
      $("#cardup").hide();
    });
    
    $( "#mixbtn" ).click(function() {
    	mix();
   	});

    $( "#sortbtn" ).click(function() {
    	sortcard();
   	});

    
    $( "#carddown-inside" ).click(function() {
    	picAcard();
    	
   	});
    
    function picAcard() {
    	numberOfCardsLeft = numberOfCardsLeft - 1;
    	$.getJSON( cardAPI+"getnextcard")
    	.done(function( json ) {

 	     $('#cardup').css('color', json.suitColor);
 	     
 	    cardLayout(json.value, json.suitHtmlChar);
 	     
 	    $("#top-left").html(json.displayName + "<br>" + json.suitHtmlChar);
 	    $("#down-left").html( json.displayName + "<br>" + json.suitHtmlChar);
 	    $("#top-right").html( json.displayName + "<br>" + json.suitHtmlChar);
 	    $("#down-right").html( json.displayName + "<br>" + json.suitHtmlChar);
 	    if (numberOfCardsLeft < 1){
 	    	$("#carddown").hide();	    	
 	    }
 	    
 	      $("#cardup").show();
 	    });
    
	}

    function sortcard() {
    	
   	   $.getJSON( cardAPI+"sortcards")
  	    .done(function( json ) {
  	      console.log( "Sort cards: " + json.numberOfCards );
  	      numberOfCardsLeft = json.numberOfCards;
  	      $("#carddown").show();	
  	      $( "#carddown-inside" ).effect( "shake" );
  	      	      
  	     $("#cardup").hide();
  	    });
     }

    
    function mix() {
    	$("#carddown").show();	
  	   $.getJSON( cardAPI+"mixcards")
 	    .done(function( json ) {
 	    console.log( "Mix cards: " + json.numberOfCards );
 	    numberOfCardsLeft = json.numberOfCards;
 	    $("#carddown").show();	
 	 	$( "#carddown-inside" ).effect( "shake" );
 	     $("#cardup").hide();
 	    });
    }
   
    function cardLayout(value, suitHtml) {
        $("#cardup-inside-center").html(""); 	
    	switch (value)
    	{
    	   case 1:
    		   $("#cardup-inside-center").html('<div id="ace">'+suitHtml+'</div>');
    		   break;
    	   case 2:
    		   $("#cardup-inside-center").html('<div id="suit-inside-top-center">'+suitHtml+'</div>');
    		   $("#cardup-inside-center").append('<div class="upside-down" id="suit-inside-bottom-center">'+suitHtml+'</div>');
    		   break;   	
    	   case 3:
    		   $("#cardup-inside-center").html('<div id="suit-inside-top-center">'+suitHtml+'</div>');
    		   $("#cardup-inside-center").append('<div id="suit-inside-center-center">'+suitHtml+'</div>');
    		   $("#cardup-inside-center").append('<div class="upside-down" id="suit-inside-bottom-center">'+suitHtml+'</div>');
    		   break;
       	   case 4:
    		   $("#cardup-inside-center").html('<div id="suit-inside-top-left">'+suitHtml+'</div>');
    		   $("#cardup-inside-center").append('<div id="suit-inside-top-right">'+suitHtml+'</div>');
    		   $("#cardup-inside-center").append('<div class="upside-down" id="suit-inside-bottom-left">'+suitHtml+'</div>');
    		   $("#cardup-inside-center").append('<div class="upside-down" id="suit-inside-bottom-right">'+suitHtml+'</div>');
    		   break;
       	   case 5:
    		   $("#cardup-inside-center").html('<div id="suit-inside-top-left">'+suitHtml+'</div>');
    		   $("#cardup-inside-center").append('<div id="suit-inside-top-right">'+suitHtml+'</div>');
    		   $("#cardup-inside-center").append('<div id="suit-inside-center-center">'+suitHtml+'</div>');
    		   $("#cardup-inside-center").append('<div class="upside-down" id="suit-inside-bottom-left">'+suitHtml+'</div>');
    		   $("#cardup-inside-center").append('<div class="upside-down" id="suit-inside-bottom-right">'+suitHtml+'</div>');
    		   break;
     	   case 6:
    		   $("#cardup-inside-center").html('<div id="suit-inside-top-left">'+suitHtml+'</div>');
    		   $("#cardup-inside-center").append('<div id="suit-inside-top-right">'+suitHtml+'</div>');
    		   
    		   $("#cardup-inside-center").append('<div id="suit-inside-center-left">'+suitHtml+'</div>');
    		   $("#cardup-inside-center").append('<div id="suit-inside-center-right">'+suitHtml+'</div>');
    		   
    		   $("#cardup-inside-center").append('<div class="upside-down" id="suit-inside-bottom-left">'+suitHtml+'</div>');
    		   $("#cardup-inside-center").append('<div class="upside-down" id="suit-inside-bottom-right">'+suitHtml+'</div>');
    		   break;
    	   case 7:
    		   $("#cardup-inside-center").html('<div id="suit-inside-top-left">'+suitHtml+'</div>');
    		   $("#cardup-inside-center").append('<div id="suit-inside-top-right">'+suitHtml+'</div>');
       		   $("#cardup-inside-center").append('<div id="suit-inside-top-center-center">'+suitHtml+'</div>');   		   
    		   $("#cardup-inside-center").append('<div id="suit-inside-center-left">'+suitHtml+'</div>');
    		   $("#cardup-inside-center").append('<div id="suit-inside-center-right">'+suitHtml+'</div>');
    		   $("#cardup-inside-center").append('<div class="upside-down" id="suit-inside-bottom-left">'+suitHtml+'</div>');
    		   $("#cardup-inside-center").append('<div class="upside-down" id="suit-inside-bottom-right">'+suitHtml+'</div>');
    		   break;
    	   case 8:
    		   $("#cardup-inside-center").html('<div id="suit-inside-top-left">'+suitHtml+'</div>');
    		   $("#cardup-inside-center").append('<div id="suit-inside-top-right">'+suitHtml+'</div>');
       		   $("#cardup-inside-center").append('<div id="suit-inside-top-center-center">'+suitHtml+'</div>');   		   
    		   $("#cardup-inside-center").append('<div id="suit-inside-center-left">'+suitHtml+'</div>');
    		   $("#cardup-inside-center").append('<div id="suit-inside-center-right">'+suitHtml+'</div>');

    		   $("#cardup-inside-center").append('<div class="upside-down" id="suit-inside-bottom-center-center">'+suitHtml+'</div>');   		   
    		   
    		   $("#cardup-inside-center").append('<div class="upside-down" id="suit-inside-bottom-left">'+suitHtml+'</div>');
    		   $("#cardup-inside-center").append('<div class="upside-down" id="suit-inside-bottom-right">'+suitHtml+'</div>');
    		   break;
    	   case 9:
    		   $("#cardup-inside-center").html('<div id="suit-inside-top-left">'+suitHtml+'</div>');
    		   $("#cardup-inside-center").append('<div id="suit-inside-top-right">'+suitHtml+'</div>');
    		   
     		   $("#cardup-inside-center").append('<div id="suit-inside-top-left-down">'+suitHtml+'</div>');
    		   $("#cardup-inside-center").append('<div id="suit-inside-top-right-down">'+suitHtml+'</div>');

    		   $("#cardup-inside-center").append('<div id="suit-inside-center-center">'+suitHtml+'</div>');

    		   
    		   $("#cardup-inside-center").append('<div class="upside-down" id="suit-inside-bottom-left-top">'+suitHtml+'</div>');   		   
    		   $("#cardup-inside-center").append('<div class="upside-down" id="suit-inside-bottom-right-top">'+suitHtml+'</div>');   		   
    		   
    		   $("#cardup-inside-center").append('<div class="upside-down" id="suit-inside-bottom-left">'+suitHtml+'</div>');
    		   $("#cardup-inside-center").append('<div class="upside-down" id="suit-inside-bottom-right">'+suitHtml+'</div>');
    		   break; 
    		case 10:
        		   $("#cardup-inside-center").html('<div id="suit-inside-top-left">'+suitHtml+'</div>');
        		   $("#cardup-inside-center").append('<div id="suit-inside-top-right">'+suitHtml+'</div>');
        		   
           		   $("#cardup-inside-center").append('<div id="suit-inside-top-center-center-max">'+suitHtml+'</div>');   	
           		   
        		   $("#cardup-inside-center").append('<div id="suit-inside-top-left-down">'+suitHtml+'</div>');
        		   $("#cardup-inside-center").append('<div id="suit-inside-top-right-down">'+suitHtml+'</div>');

        		   $("#cardup-inside-center").append('<div class="upside-down" id="suit-inside-bottom-left-top">'+suitHtml+'</div>');   		   
        		   $("#cardup-inside-center").append('<div class="upside-down" id="suit-inside-bottom-right-top">'+suitHtml+'</div>');   		   
        		   
        		   $("#cardup-inside-center").append('<div class="upside-down" id="suit-inside-bottom-center-center-max">'+suitHtml+'</div>');   	
           		   
        		   
        		   $("#cardup-inside-center").append('<div class="upside-down" id="suit-inside-bottom-left">'+suitHtml+'</div>');
        		   $("#cardup-inside-center").append('<div class="upside-down" id="suit-inside-bottom-right">'+suitHtml+'</div>');
        		   break; 
          		   break; 
    		case 11:
    			$("#cardup-inside-center").html('<img src="image/j.png" alt="J">');
    			break;
    		case 12:
    			$("#cardup-inside-center").html('<img src="image/q.png" alt="Q">');
    		break;
    		case 13:
    			$("#cardup-inside-center").html('<img src="image/k.png" alt="K">');
    			break;
		  
     	   default: 
    	     $("#cardup-inside-center").html(suitHtml); 	
    	}
    	
  	 	      	    	 
 
	}
    
});


