### Pic A Card ###
Är en webbaserad kortlek som man kan blanda och sortera samt dra det översta kortet.
Det bygger på ett API med hjälp av Jersey som körs på en Tomcat 7 server. Webbdelen körs
med hjälp av jQuery, CSS3 och Bootstrap.

![example.png](https://bitbucket.org/repo/KLAkyy/images/3847607745-example.png)

### Efter start ###
http://localhost:8080/CardGame/

API info:
http://localhost:8080/CardGame/api/application.wadl

Av Erik Lundmark